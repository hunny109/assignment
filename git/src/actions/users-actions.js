import axios from "axios";

export const UPDATE_USERNAME = "users:update_username";
export const UPDATE_OBJECTS = "users:update_objcets";
export const UPDATE_OBJECTS_PER_PAGE = "users:update_ojbects_per_page";
export const UPDATE_CURRENT_PAGE = "users:update_current_page";
export const UPDATE_TOTAL_COUNT = "users:update_totalCount";
export const UPDATE_INFO = "users:update_info";

export function updateUsername(newUsername) {
  return {
    type: UPDATE_OBJECTS,
    payload: {
      username: newUsername,
    },
  };
}

export function updateObjects(newObjects) {
  return {
    type: UPDATE_OBJECTS,
    payload: {
      objects: newObjects,
    },
  };
}

export function updateCurrentPage(newCurrentPage) {
  return {
    type: UPDATE_CURRENT_PAGE,
    payload: {
      currentPage: newCurrentPage,
    },
  };
}

export function updateObjectsPerPage(newPerPage) {
  return {
    type: UPDATE_OBJECTS_PER_PAGE,
    payload: {
      objectsPerPage: newPerPage,
    },
  };
}

export function updateTotalCount(newTotalCount) {
  return {
    type: UPDATE_TOTAL_COUNT,
    payload: {
      totalCount: newTotalCount,
    },
  };
}

export function updateInfo(newInfo) {
  return {
    type: UPDATE_INFO,
    payload: {
      info: newInfo,
    },
  };
}

export function apiRequestGetUserObjects(userName, pageNumber, perPage) {
  if (pageNumber < 1) pageNumber = 1;
  return (dispatch) => {
		dispatch(updateInfo("Loading"))
    axios
      .get(
        `https://www.myminifactory.com/api/v2/users/${userName}/objects/?page=${pageNumber}&per_page=${perPage}`,
        {
          headers: {
            Key: "8a4cc02a-0000-f14a-a815-33da126f240a",
          },
        }
      )
      .then((result) => {
        let data = result.data;
        dispatch(updateCurrentPage(pageNumber));
        dispatch(updateObjects(data.items));
        dispatch(updateTotalCount(data.total_count));
				dispatch(updateInfo(""))
      })
      .catch((error) => console.log(error));
  };
}
