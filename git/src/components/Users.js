import React, { Component } from 'react'

// import logo from './logo.svg';

import './App.css';
import { connect } from 'react-redux';
import { apiRequestGetUserObjects } from './actions/users-actions'
import { createSelector } from 'reselect'

class Users extends Component {
  featureObjects = false // render featured only objects

  constructor(props) {
    super(props)
    this.getUserObjects = this.getUserObjects.bind(this)
    this.handleNextPage = this.handleNextPage.bind(this)
    this.handlePreviousPage = this.handlePreviousPage.bind(this)
  }

  componentDidMount() {
    this.getUserObjects(this.props.users.currentPage)
  }

  handleNextPage() {
    if(this.props.users.totalCount < this.props.users.objectsPerPage){
      return
    }
    this.getUserObjects(this.props.users.currentPage+1)
  }

  handlePreviousPage() {
    if(this.props.users.currentPage === 1){
      return
    }
    this.getUserObjects(this.props.users.currentPage-1)
  }

  getUserObjects(pageNumber)
  {
    this.props.onApiRequestGetUserObjects(
        this.props.match.params.username,
        pageNumber,
        this.props.users.perPage
    )
  }

  render() {
    if(this.featureObjects){
      return (
        <div>
          <div>Hello world</div>
          <div>
            {this.props.user.objects.filter(object => object.featured).map(featureObject => (
              <div key={featureObject.id}>
                <div>{featureObject.name}</div>
              </div>
            ))}
          </div>
          {
              this.props.users.objects.map((object) => (
                  <div key={object.id}>
                    <div>{object.name}</div>
                  </div>
              ))
          }
          <div onClick={this.handlePreviousPage}>Previous</div>
          <div onClick={this.handleNextPage}>Next</div>
        </div>
      )
    }

    return (
      <div >
        <div>Hello world</div>
        {
            this.props.users.objects.map((object) => (
                <div key={object.id}>
                  <div>{object.name}</div>
                </div>
            ))
        }
        <div onClick={this.handlePreviousPage}>Previous</div>
        <div onClick={this.handleNextPage}>Next</div>
      </div>
    )

  }
}

const usersSelector = createSelector(
    state => state.users,
    users => users
)

const mapStateToProps = createSelector(
    usersSelector,
    (users) => ({
      users,
    })
);
const mapActionsToProps = {
  onApiRequestGetUserObjects: apiRequestGetUserObjects,
}

export default connect(mapStateToProps, mapActionsToProps)(Users)
