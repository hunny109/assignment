import React, { Component } from "react";

// import logo from './logo.svg';

import "../App.css";

class Item extends Component {
  render() {
    return (
      <div className="card">
        <img
          className="pic"
          src={this.props.object.images[0].thumbnail.url}
          alt=""
        />
        <div>{this.props.object.name}</div>
      </div>
    );
  }
}

export default Item;
