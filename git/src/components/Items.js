import React, { Component } from 'react'

// import logo from './logo.svg';
import Item from './Item'

import '../App.css';

class Items extends Component {
  render() {
    if (this.props.featured) {
      return (
        <div>
          <div>
            {this.props.users.objects
              .filter((object) => object.featured)
              .map((featureObject) => (
                <Item object={featureObject} />
              ))}
          </div>
        </div>
      );
    }

    return (
      <div>
        {this.props.users.objects.map((object) => (
            <Item object={object} />
        ))}
      </div>
    );
  }
}

export default Items
