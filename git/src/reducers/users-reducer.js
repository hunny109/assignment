import {
	UPDATE_USERNAME,
	UPDATE_OBJECTS,
  UPDATE_CURRENT_PAGE,
  UPDATE_OBJECTS_PER_PAGE,
  UPDATE_TOTAL_COUNT,
	UPDATE_INFO,
} from '../actions/users-actions'

export default function usersReducer(
  state = {
		username:'myminifactory',
    objects: [],
    currentPage: 18,
    objectsPerPage: 20,
    totalCount: 0,
		info:""
  }, {
    type,
    payload
  }) {

  switch (type) {
		case UPDATE_USERNAME:
      return {
        ...state, username: payload.username
      }
    case UPDATE_OBJECTS:
      return {
        ...state, objects: payload.objects
      }
    case UPDATE_CURRENT_PAGE:
      return {
        ...state, currentPage: payload.currentPage
      }
    case UPDATE_OBJECTS_PER_PAGE:
      return {
        ...state, objectsPerPage: payload.objectsPerPage
      }
    case UPDATE_TOTAL_COUNT:
      return {
        ...state, totalCount: payload.totalCount
      }
		case UPDATE_INFO:
	    return {
	      ...state, info: payload.info
	    }
    default:
        return state
  }
}
