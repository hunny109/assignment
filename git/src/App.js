import React, { Component } from "react";

// import logo from './logo.svg';

import "./App.css";
import { connect } from "react-redux";
import { updateInfo, apiRequestGetUserObjects } from "./actions/users-actions";
import { createSelector } from "reselect";
import Items from "./components/Items";

class App extends Component {
  state = {
    featured: false,
  };
  constructor(props) {
    super(props);
    this.getUserObjects = this.getUserObjects.bind(this);
    this.handleNextPage = this.handleNextPage.bind(this);
    this.handlePreviousPage = this.handlePreviousPage.bind(this);
    this.handleToggleFeatured = this.handleToggleFeatured.bind(this);
  }

  componentDidMount() {
    this.getUserObjects(this.props.users.currentPage);
  }

  handleNextPage() {
    if (
      this.props.users.totalCount <=
      (this.props.users.objectsPerPage * this.props.users.currentPage)
    ) {
      return this.props.onUpdateInfo("No Next Page");
    }

    this.getUserObjects(this.props.users.currentPage + 1);
  }

  handlePreviousPage() {
    if (this.props.users.currentPage === 1) {
      return  this.props.onUpdateInfo("No Previous Page");
    }
    this.getUserObjects(this.props.users.currentPage - 1);
  }

  handleToggleFeatured() {
    if (this.state.featured) {
      this.setState({ featured: false });
    } else {
      this.setState({ featured: true });
    }
  }

  getUserObjects(pageNumber) {
    this.props.onApiRequestGetUserObjects(
      this.props.users.username,
      pageNumber,
      this.props.users.objectsPerPage
    );
  }

  render() {
    return (
      <div>
        <h3>{this.props.users.username}</h3>
        <div
          className={
            this.state.featured ? "button button-black" : "button button-gray"
          }
          onClick={this.handleToggleFeatured}
        >
          Featured Only
        </div>
        <Items featured={this.state.featured} users={this.props.users} />
        <div className="info">{this.props.users.info}</div>
        <div className={"button button-gray"} onClick={this.handlePreviousPage}>
          Previous
        </div>
        <div className={"button button-gray"} onClick={this.handleNextPage}>
          Next
        </div>
      </div>
    );
  }
}

const usersSelector = createSelector(
  (state) => state.users,
  (users) => users
);

const mapStateToProps = createSelector(usersSelector, (users) => ({
  users,
}));
const mapActionsToProps = {
  onApiRequestGetUserObjects: apiRequestGetUserObjects,
  onUpdateInfo: updateInfo
};

export default connect(mapStateToProps, mapActionsToProps)(App);
